import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  patient = [
    {
      code : 52,
      name : "PHẠM ANH QUÂN",
      age : 42,
      sche : "9:45 AM"

    },
    {
      code : 54,
      name : "HOÀNG VÂN ANH",
      age : 35,
      sche : "10:15 AM"

    },
    {
      code : 56,
      name : "PHẠM THỊ TRANG",
      age : 28,
      sche : "10:25 AM"

    },
    {
      code : 57,
      name : "LÊ VĂN HẢI",
      age : 35,
      sche : "11:15 AM"

    },
    {
      code : 58,
      name : "NGUYỄN MINH HOÀNG",
      age : 3305,
      sche : "11:30 AM"

    }
  ];

  wait = [
    {
      code : 40,
      name : "TRẦN THỊ HỒNG",
      age : 42,
      sche : "9:45 AM"

    },
    {
      code : 42,
      name : "LÊ TIẾN DŨNG",
      age : 35,
      sche : "10:15 AM"

    },
    {
      code : 44,
      name : "NGUYỄN THỊ QUỲNH",
      age : 28,
      sche : "10:25 AM"

    },
    
  ]

  ngOnInit(): void {
    const interval = setInterval(() => {
      this.shuff();
    }, 5000);
  }


  shuff(){
    this.patient = this.patient.sort(() => Math.random() - 0.5);
    this.wait = this.wait.sort(() => Math.random() - 0.5);
  }
  title = 'my-queue-app';


}
